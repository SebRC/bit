public class Time
{

public static void main(String[] args)
{

int hour = 24;
int min = 0;
int sec = 0;
int secperday = 86400;
int secsincemid = hour * 60 * 60 + min *60 + sec;
int secremain = secperday - secsincemid;
int percentage = ((secsincemid * 100) / secperday);

System.out.println(secsincemid + " seconds since midnight.");
System.out.println(secremain + " seconds till midnight.");
System.out.println(percentage + "% of the day has passed.");

}

}